import QtQuick 2.0
import QtQuick.Layouts 1.0

Item {
    id: addChannelWindow
    Rectangle {
        width: 324
        height: 192
        radius: 25
        color: "#cf0f0f0f"
        anchors {
            top: parent.top
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }

        //ShaderEffect {
        //    anchors.fill: parent
        //    property variant src: parent
        //    vertexShader: "
        //        uniform highp mat4 qt_Matrix;
        //        attribute highp vec4 qt_Vertex;
        //        attribute highp vec2 qt_MultiTexCoord0;
        //        varying highp vec2 coord;
        //        void main() {
        //            coord = qt_MultiTexCoord0;
        //            gl_Position = qt_Matrix * qt_Vertex;
        //        }"
        //    fragmentShader: "
        //        varying highp vec2 coord;
        //        uniform sampler2D src;
        //        uniform lowp float qt_Opacity;
        //        float normpdf(in float x, in float sigma) {
        //            return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
        //        }
        //        void main() {
        //            const int mSize = 11;
        //            const int kSize = (mSize-1)/2;
        //            float kernel[mSize];
        //            vec3 final_colour = vec3(0.0);

        //            float sigma = 7.0;
        //            float Z = 0.0;
        //            for (int j = 0; j <= kSize; ++j) {
        //                kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), sigma);
        //            }

        //            //get the normalization factor (as the gaussian has been clamped)
        //            for (int j = 0; j < mSize; ++j) {
        //                Z += kernel[j];
        //            }

        //            //read out the texels
        //            for (int i=-kSize; i <= kSize; ++i) {
        //                for (int j=-kSize; j <= kSize; ++j) {
        //                    final_colour += kernel[kSize+j]*kernel[kSize+i]*texture2D(src, coord.xy + (vec2(float(i),float(j)) / 300.0)).rgb;
        //                }
        //            }
        //            gl_FragColor = vec4(final_colour/(Z*Z), 1.0) * qt_Opacity;
        //        }"
        //}

        ListView {
            id: channelFullList
            anchors.fill: parent
            model: AllChannelListModel
            Connections {
                target: rcAPI
                onListAvailableChannel: {
                   AllChannelListModel.addChannel(channelId, channelName, update, 0)
                }
            }

            delegate: Item {
                x: 5
                width: 80
                height: 40
                anchors {
                   left: parent.left
                   right: parent.right
                }
                RowLayout {
                    id: row1
                    anchors {
                       left: parent.left
                       leftMargin: 40
                       right: parent.right
                    }
                    Rectangle {
                        id: background
                        radius: 10
                        height: 40
                        width: channelListView.width
                        //color: "#6b6b6b"
                        color: mouseArea.containsMouse ? "#5f6b6b6b" : "#0f6b6b6b"
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        Rectangle {
                            id: channelIcon
                            width: 40
                            height: 40
                            radius: 10
                            color: colorCode
                            Text {
                                color: "#ffffff"
                                text: model.name[0]
                                font.capitalization: Font.AllUppercase
                                font.pointSize: 12
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                anchors.fill: parent
                            }
                        }
                        Text {
                            id: chanName
                            text: model.name
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: channelIcon.right
                            anchors.leftMargin: 5
                            color: "#9da097"
                        }
                        MouseArea {
                            id: mouseArea
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                ChannelListModel.addChannel(model._id, model.name, model.update,model.type)
                                //https://rocket.chat/docs/developer-guides/rest-api/channels/open/
                            }
                        }
                    }
                    spacing: 10
                }
            }


        }
    }
}
