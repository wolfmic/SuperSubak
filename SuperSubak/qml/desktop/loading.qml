import QtQuick 2.2

Item {
    id: loadingAnim
    Rectangle {
        id: rotatingRect
        x: 220
        y: 140
        width: 400
        height: 400
        color: "#00ffffff"
        radius: 120
        enabled: false
        border.color: "#253e7a"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        border.width: 16
        ParallelAnimation {
            id: internalAnim
            running: true
            RotationAnimation {
                target: rotatingRect
                loops: Animation.Infinite
                from:0
                to: 90
            }
            ScaleAnimator {
                target: rotatingRect
                from: 0
                to: 1
                duration: 200
            }
        }
    }

    Grid {
        id: grid
        width: 400
        height: 400
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:5;anchors_height:200;anchors_width:200;anchors_x:179;anchors_y:182}
}
 ##^##*/
