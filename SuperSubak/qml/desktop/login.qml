import QtQuick 2.2
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2

Item {
    id: loginScreen
    //color: "#212128"
    anchors.fill: parent

    Connections {
        target: rcAPI
        onLoginFailed: {
            loadingAnimLoader.source = ""
        }
    }

    //ShaderEffect {
    //    anchors.fill: parent
    //    //property variant src: parent
    //    property variant src: Image {
    //        anchors.fill: parent
    //        source: "qrc:/watermelon-wallpaper1.jpg"
    //    }
    //    //property variant src: ShaderEffectSource {
    //    //   id: texSource
    //    //   sourceItem: rootWindow
    //    //   width: sourceItem.width
    //    //   height: sourceItem.height
    //    //}

    //    vertexShader: "
    //        #version 330
    //        in highp vec4 qt_Vertex;
    //        in highp vec2 qt_MultiTexCoord0;
    //        out highp vec2 coord;
    //        uniform highp mat4 qt_Matrix;
    //        void main() {
    //            coord = qt_MultiTexCoord0;
    //            gl_Position = qt_Matrix * qt_Vertex;
    //        }"
    //    fragmentShader: "
    //        #version 330
    //        in highp vec2 coord;
    //        out highp vec4 colour;
    //        uniform sampler2D src;
    //        uniform lowp float qt_Opacity;

    //        vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
    //            vec4 color = vec4(0.0);
    //            vec2 off1 = vec2(1.411764705882353) * direction;
    //            vec2 off2 = vec2(3.2941176470588234) * direction;
    //            vec2 off3 = vec2(5.176470588235294) * direction;
    //            color += texture(image, uv) * 0.1964825501511404;
    //            color += texture(image, uv + (off1 / resolution)) * 0.2969069646728344;
    //            color += texture(image, uv - (off1 / resolution)) * 0.2969069646728344;
    //            color += texture(image, uv + (off2 / resolution)) * 0.09447039785044732;
    //            color += texture(image, uv - (off2 / resolution)) * 0.09447039785044732;
    //            color += texture(image, uv + (off3 / resolution)) * 0.010381362401148057;
    //            color += texture(image, uv - (off3 / resolution)) * 0.010381362401148057;
    //            return color;
    //        }

    //        void main() {
    //            colour = blur13(src, coord, vec2(1000.0, 1000.0), vec2(2.0, 2.0));
    //            //colour = vec4(0.0, 1.0, 0.0, 1.0);
    //        }"

    //    //fragmentShader: "
    //    //    #version 330
    //    //    in highp vec2 coord;
    //    //    out highp vec4 colour;
    //    //    uniform sampler2D src;
    //    //    uniform lowp float qt_Opacity;
    //    //    mediump float normpdf(in mediump float x) {
    //    //        const highp float sigma = 7.0;
    //    //        return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
    //    //    }
    //    //    void main() {
    //    //        const int mSize = 11;
    //    //        const int kSize = (mSize-1)/2;
    //    //        mediump float kernel[mSize];
    //    //        mediump float Z = 0.0;

    //    //        colour = vec4(1.0, 1.0, 1.0, 1.0);
    //    //        return;

    //    //        for (int j = 0; j <= kSize; ++j) {
    //    //            kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j));
    //    //        }

    //    //        //get the normalization factor (as the gaussian has been clamped)
    //    //        for (int j = 0; j < mSize; ++j) {
    //    //            Z += kernel[j];
    //    //        }

    //    //        //read out the texels
    //    //        vec4 final_colour = vec4(0.0, 0.0, 0.0, 1.0);
    //    //        for (int i=-kSize; i <= kSize; ++i) {
    //    //            for (int j=-kSize; j <= kSize; ++j) {
    //    //                final_colour += kernel[kSize+j]*kernel[kSize+i]*texture(src, coord.xy + (vec2(float(i),float(j)) / 300.0));
    //    //            }
    //    //        }
    //    //        colour = vec4(final_colour.xyz/(Z*Z), 1.0);
    //    //    }"
    //}

    Item {
        id: column
        y: 145
        width: 1263
        height: 429
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Rectangle {
            id: server
            height: 30
            //color: "#99e56464"
            color: "#995B5B5E"
            radius: 50
            anchors {
                left: parent.left
                leftMargin: 50
                right: parent.right
                rightMargin: 50
                verticalCenterOffset: -170
                verticalCenter: parent.verticalCenter
            }
            Text {
                id: http
                height: server.height
                font.pixelSize: 20
                color: "#60ffffff"
                text: qsTr("https://")
                textFormat: Text.PlainText
                anchors {
                    left: parent.left
                    leftMargin: 10
                    verticalCenter: parent.verticalCenter
                }
            }
            TextEdit {
                id: serverInput
                height: server.height
                text: qsTr("rc.subak.ovh")
                anchors.left: http.right
                //anchors.leftMargin: 72
                anchors.leftMargin: 2
                anchors.right: parent.right
                anchors.rightMargin: 30
                anchors.verticalCenter: parent.verticalCenter
                selectionColor: "#803500"
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 20
                selectByMouse: true

                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: loginInput
                KeyNavigation.up: passInput
                KeyNavigation.down: loginInput
                onFocusChanged: {
                    if (activeFocus == true) {
                        selectAll();
                    } else if (activeFocus == false) {
                        assetLogo.source = rcAPI.getAssetLogo(serverInput.text);
                    }
                }
                Keys.onReturnPressed: {
                    loadingAnimLoader.source = "qrc:/qml/desktop/loading.qml";
                    rcAPI.login(loginInput.text, passInput.text, serverInput.text);
                }
            }
        }

        Rectangle {
            id: login
            height: 60
            radius: 10
            color: "#995B5B5E"
            //color: "#99e56464"
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.verticalCenterOffset: -50
            anchors.verticalCenter: parent.verticalCenter
            TextEdit {
                id: loginInput
                height: 44
                //text: qsTr("Login")
                property string placeholderText: "login"
                Text {
                    text: parent.placeholderText
                    anchors.fill: parent
                    lineHeight: 0.7
                    color: "#60ffffff"
                    font.pixelSize: parent.font.pixelSize
                    visible: !parent.text
                }

                clip: true
                selectionColor: "#803500"
                anchors.left: login.left
                anchors.leftMargin: 20
                anchors.right: login.right
                anchors.rightMargin: 20
                anchors.verticalCenter: login.verticalCenter
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 30
                selectByMouse: true
                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: passInput
                KeyNavigation.down: passInput
                KeyNavigation.up: serverInput
                focus: {forceActiveFocus(); return true}
                onFocusChanged: {
                    if (activeFocus == true) {
                        selectAll()
                    }
                }

                Keys.onReturnPressed: {
                    loadingAnimLoader.source = "qrc:/qml/desktop/loading.qml"
                    rcAPI.login(loginInput.text, passInput.text, serverInput.text)
                }
            }
        }
        Rectangle {
            id: pass
            height: 60
            radius: 10
            color: "#995B5B5E"
            //color: "#99e56464"
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.verticalCenterOffset: 50
            anchors.verticalCenter: parent.verticalCenter

            TextInput {
                id: passInput
                height: 44
                property string placeholderText: "Password"
                Text {
                    text: parent.placeholderText
                    anchors.fill: parent
                    lineHeight: 0.7
                    color: "#60ffffff"
                    font.pixelSize: parent.font.pixelSize
                    visible: !parent.text
                }
                font.family: "Verdana"
                echoMode: TextInput.Password
                horizontalAlignment: Text.AlignLeft
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 30
                selectByMouse: true
                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: serverInput
                KeyNavigation.down: serverInput
                KeyNavigation.up: loginInput
                onFocusChanged: {
                    if (activeFocus == true) {
                        selectAll()
                    }
                }
                Keys.onReturnPressed: {
                    loadingAnimLoader.source = "qrc:/qml/desktop/loading.qml"
                    rcAPI.login(loginInput.text, passInput.text, serverInput.text)
                }
            }
        }

        Rectangle {
            id: connect
            width: 200
            height: 50
            radius: 15
            color: "#995B5B5E"
            //color: "#ccf44b4b"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            transformOrigin: Item.Center
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onClicked: {
                    loadingAnimLoader.source = "qrc:/qml/desktop/loading.qml"
                    rcAPI.login(loginInput.text, passInput.text, serverInput.text)
                }
                Text {
                    text: qsTr("Connect")
                    font.family: "Verdana"
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 30
                }
            }

        }

        Image {
            id: assetLogo
            height: 115
            sourceSize.width: 128
            sourceSize.height: 128
            fillMode: Image.PreserveAspectFit
            anchors {
                right: connect.left
                rightMargin: 20
                left: parent.left
                leftMargin: 24
                bottom: parent.bottom
                bottomMargin: 20
            }
            source: rcAPI.getAssetLogo(serverInput.text)
        }
    }

    Loader {
        id: loadingAnimLoader
        anchors.fill: parent
        asynchronous: true
    }


}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:11;anchors_height:100;anchors_width:100;anchors_x:"-15";anchors_y:54}
}
 ##^##*/
