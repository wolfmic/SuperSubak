import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0

Rectangle {
    id: chatScreen
    anchors {
        fill: parent
    }
    color: "#1e262f"
    Rectangle {
        id: serverLayout
        color: "#312845"
        width: 0
        visible: false
        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        Shortcut {
            sequences: ["Alt+s", "Ctrl+s"]
            onActivated: {
                serverLayout.visible = !serverLayout.visible;
                if (!serverLayout.visible) {
                    serverLayout.width = 0;
                } else {
                    serverLayout.width = 50;
                }
            }
        }
        Behavior on width {
            NumberAnimation {
                duration: 200
                easing.type: Easing.InOutQuad
            }
        }
        ListView {
            id: servers
            anchors {
                fill: parent
            }
            delegate: Item {
                width: 50
                height: 52
                Rectangle {
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        verticalCenter: parent.verticalCenter
                    }
                    color: "#623d75"
                    radius: 5
                    width: 48
                    height: 48
                    Text {
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            verticalCenter: parent.verticalCenter
                        }
                        text: name
                    }
                }
            }
            model: ServerListModel
            //model: ListModel {
            //    ListElement { name:"serv1" }
            //    ListElement { name:"serv2" }
            //    ListElement { name:"serv3" }
            //}
        }
    }

    Item {
        id: selectorLayout
        width: 172
        visible: true
        anchors {
            top: parent.top
            topMargin: 0
            bottom: parent.bottom
            bottomMargin: 0
            left: serverLayout.right
            leftMargin: 0
        }
        Behavior on width {
            NumberAnimation {
                duration: 100
                easing.type: Easing.InOutQuad
            }
        }

        Shortcut {
            sequences: ["Alt+n", "Ctrl+n"]
            onActivated: {
                selectorLayout.visible = !selectorLayout.visible;
                if (!selectorLayout.visible) {
                    selectorLayout.width = 0;
                } else {
                    selectorLayout.width = 172;
                }
            }
        }


        Rectangle {
            id: channelBackground
            color: "#2f343d"
            radius: 0
            anchors {
                fill: parent
            }

            Rectangle {
                id: addChannelBt
                height: 21
                color: "#646464"
                radius: 5
                anchors {
                    left: parent.left
                    bottom: parent.bottom
                    bottomMargin: 15
                    right: parent.right
                    rightMargin: 8
                    leftMargin: 12
                }
                property bool opened: false
                Text {
                    color: "#ffffff"
                    text: addChannelBt.opened ? "-" : "+"
                    lineHeight: 1.1
                    font.bold: true
                    anchors {
                        topMargin: 0
                        fill: parent
                    }
                    font.pointSize: 12
                    font.family: "Verdana"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea {
                    id: mouseArea1
                    anchors {
                        fill: parent
                    }
                    onClicked: {
                        if (addChannelBt.opened) {
                            //addChannelpopup.active = true;
                            addChannelpopup.source = "";
                           addChannelBt.opened = false;
                        } else {
                            //addChannelpopup.active = true;
                            addChannelpopup.source = "qrc:/qml/desktop/addChannel.qml";
                            AllChannelListModel.clear();
                            rcAPI.listAllChannels();
                            addChannelBt.opened = true;
                        }
                    }
                }
            }

            Image {
                id: logo
                height: 100
                sourceSize.width: 64
                sourceSize.height: 64
                anchors {
                    top: parent.top
                    topMargin: -25
                    right: parent.right
                    rightMargin: 0
                    left: parent.left
                    leftMargin: 0
                }
                fillMode: Image.PreserveAspectFit
                source: rcAPI.getAssetLogo()
            }

            ListView {
                id: channelListView
                x: 15
                clip: true
                anchors {
                    topMargin: -15
                    right: parent.right
                    left: parent.left
                    top: logo.bottom
                    bottomMargin: 15
                    bottom: addChannelBt.top
                }
                onCurrentItemChanged: {
                    messageView.model.clear()
                    rcAPI.selectChannel(model.getId(currentIndex), model.getName(currentIndex), model.getType(currentIndex))
                }
                highlight: Rectangle {
                    height: 40
                    width: channelListView.width
                    color: "#995d5d5d"
                    y: channelListView.currentItem.y
                    Behavior on y { SmoothedAnimation { velocity: 300 } }
                }
                Shortcut {
                    sequence: "PgDown"
                    onActivated: {
                        if (channelListView.currentIndex != (channelListView.count - 1)) {
                            channelListView.incrementCurrentIndex()
                        } else {
                            channelListView.currentIndex = 0;
                        }
                    }
                }
                Shortcut {
                    sequence: "PgUp"
                    onActivated: {
                        if (channelListView.currentIndex != 0) {
                            channelListView.decrementCurrentIndex()
                        } else {
                            channelListView.currentIndex = (channelListView.count - 1);
                        }
                    }
                }

                highlightFollowsCurrentItem: false
                model: ChannelListModel
                section {
                    property: "type"
                    criteria: ViewSection.FullString
                        delegate: Item {
                            height: 15
                            Text {
                                color: "#9da097"
                                font.bold: true
                                text: section == 1 ? "DM" : "Channels"
                            }
                        }
                }
                delegate: Item {
                    x: 5
                    width: 80
                    height: 40
                    RowLayout {
                        id: row1
                        Rectangle {
                            id: background
                            radius: 10
                            height: 40
                            width: channelListView.width - 5
                            //color: "#6b6b6b"
                            color: mouseArea.containsMouse ? "#5f6b6b6b" : "#0f6b6b6b"
                            Rectangle {
                                id: channelIcon
                                width: 40
                                height: 40
                                radius: 10
                                color: colorCode
                                Text {
                                    color: "#ffffff"
                                    text: model.name[0]
                                    font.capitalization: Font.AllUppercase
                                    font.pointSize: 12
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                    anchors {
                                        fill: parent
                                    }
                                }
                            }
                            Text {
                                id: chanName
                                text: model.name
                                font.bold: model.lastUpdate < new Date()
                                anchors {
                                    verticalCenter: parent.verticalCenter
                                    left: channelIcon.right
                                    leftMargin: 5
                                }
                                color: "#9da097"
                            }
                            Rectangle {
                                width: 30
                                height: 15
                                radius: 5
                                color: "#b94747"
                                anchors {
                                    verticalCenter: parent.verticalCenter
                                    right: parent.right
                                }
                                visible: model.notificationAmount > 0
                                Text {
                                    text: model.notificationAmount
                                    height:10
                                    anchors {
                                        verticalCenterOffset: -2
                                        verticalCenter: parent.verticalCenter
                                        horizontalCenter: parent.horizontalCenter
                                    }
                                    color: "#9da097"
                                }
                            }
                            MouseArea {
                                id: mouseArea
                                anchors {
                                    fill: parent
                                }
                                hoverEnabled: true
                                onClicked: {
                                    console.log("onClicked");
                                    channelListView.currentIndex = index
                                    messageView.model.clear()
                                    rcAPI.selectChannel(model._id, model.name, model.type)
                                }
                            }
                        }
                        spacing: 10
                    }
                }
                Connections {
                    target: rcAPI
                    onAddJoinedChannel: {
                       ChannelListModel.addChannel(channelId, channelName, lastUpdated, type);
                    }
                    onUpdateChannel: {
                       ChannelListModel.setChannel(channelId, notif, update);
                    }
                }
            }
        }

    }
    Item {
        id: chatLayout
        anchors {
            left: selectorLayout.right
            leftMargin: 0
            right: parent.right
            rightMargin: 0
            top: parent.top
            topMargin: 0
            bottom: parent.bottom
            bottomMargin: 0
        }

        Rectangle {
            id: loadMoreFeedBack
            width: 250
            height: 30
            color: "#384268"
            radius: 15
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: -30
            }
            Text {
                color: "#9da097"
                text: "⭮"
                //text: "loadingMore"
                font.family: "Verdana"
                font.pointSize: 8
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.bottom
                    bottomMargin: 1
                }
            }
            Behavior on anchors.topMargin {
                NumberAnimation {
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
        }
        
        ListView {
            id: messageView
            //interactive: false
            clip: true
            onIsAtBoundaryChanged: {
                if (atYBeginning == true) {
                    loadMoreFeedBack.anchors.topMargin = -15;
                    //rcAPI.loadMore();
                } else {
                    loadMoreFeedBack.anchors.topMargin = -30;
                }
            }
            anchors {
                bottom: chatZone.top
                top: parent.top
                left: parent.left
                right: parent.right
            }
            //verticalLayoutDirection: ListView.BottomToTop
            snapMode: ListView.NoSnap
            orientation: ListView.Vertical
            model: MessageListModel

            Shortcut {
                sequences: ["Alt+e", "Ctrl+e"]
                onActivated: {
                    if (messageView.avatarSize == 40) {
                        messageView.avatarSize = 0;
                    } else {
                        messageView.avatarSize = 40;
                    }
                }
            }
            property int avatarSize: 40
            Behavior on avatarSize {
                NumberAnimation {
                    duration: 100
                    easing.type: Easing.InOutQuad
                }
            }
            section {
                property: "id_author"
                criteria: ViewSection.FullString
                delegate: Item {
                        id: row
                        width: messageView.width
                        //height: (textMessage.paintedHeight < 60) ? 60 : (textMessage.paintedHeight + 30)
                        height: 20
                        Item {
                            anchors {
                                fill: parent
                            }
                            Text {
                                id: loadPlaceHolder
                                width: messageView.avatarSize
                                height: messageView.avatarSize
                                text: "⭮"
                                color: "#9da097"
                                visible: avatarImage.status == Image.Error
                                font {
                                    family: "Verdana"
                                    pointSize: 32
                                }
                                anchors {
                                    left: parent.left
                                    leftMargin: 10
                                    top: parent.top
                                    bottomMargin: 15
                                }
                                RotationAnimation {
                                    running: true
                                    target: loadPlaceHolder
                                    duration: 1000
                                    loops: Animation.Infinite
                                    from:0
                                    to: 360
                                }
                            }
                            Image {
                                id: avatarImage
                                visible: messageView.avatarSize > 1
                                width: messageView.avatarSize
                                height: messageView.avatarSize
                                asynchronous: true
                                mipmap: true
                                sourceSize.width: 64
                                sourceSize.height: 64
                                source: "file:/" + rcAPI.getCacheFolder() + "/" + section.slice(0, 17)
                                anchors {
                                    left: parent.left
                                    leftMargin: 10
                                    top: parent.top
                                    bottomMargin: 15
                                }
                            }
                            Text {
                                id: authorName
                                text: section.slice(17)
                                color: "#7c628c"
                                anchors {
                                    left: parent.left
                                    leftMargin:messageView.avatarSize + 15;
                                    top: parent.top
                                    bottomMargin: 15
                                }
                                font.bold: true
                            }
                    }
                }
            }
            delegate: Item {
                    id: row
                    width: messageView.width
                    height: (textMessage.paintedHeight < 5) ? 5 + attachmentBloc.height : textMessage.paintedHeight + attachmentBloc.height
                    Item {
                        anchors {
                            fill: parent
                        }
                        Rectangle {
                            id: attachmentBloc
                            height: attachmentType == 1 ? 100 : replyAttachment.paintedHeight + 5
                            anchors {
                                top: textMessage.bottom
                                left: parent.left
                                leftMargin: messageView.avatarSize + 40
                                right: parent.right
                                rightMargin: 10
                            }
                            visible: attachmentType > 0
                            color: "#121075bf"
                            Image {
                                visible: attachmentType == 1 // imageAttachment
                                source: attachment
                                fillMode: Image.PreserveAspectFit
                                sourceSize.width: 128
                                sourceSize.height: 128
                                anchors {
                                    left: parent.left
                                    leftMargin: 5
                                    top: parent.top
                                    topMargin: 5
                                    bottom: parent.bottom
                                    bottomMargin: 5
                                }
                            }
                            Text {
                                id: replyAttachment
                                visible: attachmentType == 2 // reply attachment
                                text: attachment
                                color: "#decfcf"
                                anchors {
                                    left: parent.left
                                    top: parent.top
                                    bottom: parent.bottom
                                }
                            }
                        }
                        Text {
                            id: textMessage
                            color: "#decfcf"
                            text: message
                            fontSizeMode: Text.VerticalFit
                            textFormat: Text.RichText
                            wrapMode: Text.Wrap
                            //width: parent.width
                            anchors {
                                 left: parent.left
                                 leftMargin: messageView.avatarSize + 30
                                 top: parent.top
                                 //top: authorName.bottom
                                 //topMargin: 5
                                 right: parent.right
                                 rightMargin: 10
                                 //bottom: parent.bottom
                            }
                            onLinkActivated: {
                                console.log(link);
                                if (!Qt.openUrlExternally(link)) {
                                    rcAPI.fallbackOpenLink(link)
                                }
                            }
                            linkColor: "#0085b7"
                        }
                }
            }
            Connections {
                target: rcAPI
                onRecievedMessage: {
                    // textMessage.paintedWidth for multiline
                    MessageListModel.addMessage(messageId, messageString, attachmentString, authorName, userid, attachmentType, oldMode)
                    gc()
                }
                onGoToEnd: {
                    messageView.positionViewAtEnd();
                }
            }
        }

        Rectangle {
            id: userCompletion
            color: "#312845"
            height: 15
            visible: chatBox.isCompleting
            anchors {
                right: parent.right
                left: parent.left
                bottom: chatZone.top
            }
            Behavior on height {
                NumberAnimation {
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
            ListView {
                id: userCompletionView
                anchors {
                    right: parent.right
                    left: parent.left
                    bottom: parent.bottom
                    top: parent.top
                }
                orientation: ListView.Horizontal
                delegate: Item {
                    width: 100
                    height: parent.height // could not make an anchor because of a QT but
                    Text {
                        text: name
                    }
                }
                //Shortcut {
                //    sequence: "tab"
                //    onActivated: {
                //        if (userCompletionView.currentIndex != (userCompletionView.count - 1)) {
                //            userCompletionView.incrementCurrentIndex()
                //        } else {
                //            userCompletionView.currentIndex = 0;
                //        }
                //    }
                //}
                highlight: Rectangle {
                    color: "#623d75"
                }
                model: UserListModel
            }
        }
        Rectangle {
            id: chatZone
            //height: 60
            height: (chatBox.paintedHeight < 60) ? 60 : (chatBox.paintedHeight + 30)
            color: "#424242"
            anchors {
                left: parent.left
                leftMargin: 0
                right: parent.right
                rightMargin: 0
                bottom: parent.bottom
                bottomMargin: 0
            }

            TextEdit {
                id: chatBox
                color: "#ededed"

                property string placeholderText: "Hi !"
                Text {
                    text: parent.placeholderText
                    lineHeight: 0.7
                    color: "#4cffffff"
                    font.pixelSize: parent.font.pixelSize
                    visible: !parent.text
                }
                selectionColor: "#181c28"
                wrapMode: Text.WordWrap
                anchors {
                    rightMargin: 10
                    leftMargin: 10
                    bottomMargin: 10
                    topMargin: 10
                    fill: parent
                }
                horizontalAlignment: Text.AlignLeft
                font.family: "Verdana"
                textFormat: Text.AutoText
                transformOrigin: Item.Bottom
                font.pixelSize: 12
                selectByMouse: true
                focus: true
                property bool isCompleting: false
                Keys.onReturnPressed: {
                    if (isCompleting) {
                        // TODO insert under cursor instead of end here  
                        insert(cursorPosition, "@" + UserListModel.getName(userCompletionView.currentIndex) + " ");
                        //text += UserListModel.getName(userCompletionView.currentIndex) + " ";
                        isCompleting = false;
                    } else {
                        rcAPI.postmessage(chatBox.text);
                        chatBox.clear();
                    }
                }
                Keys.onTabPressed: {
                    // TODO: check if character under cursor is a @ here
                    if (userCompletionView.currentIndex != (userCompletionView.count - 1)) {
                        userCompletionView.incrementCurrentIndex()
                    } else {
                        userCompletionView.currentIndex = 0;
                    }
                    isCompleting = true;
                }
            }
        }
    }

    Loader {
        id: addChannelpopup
        anchors {
            fill: parent
        }
        asynchronous: true
        //active: false
        onLoaded: {
            if (addChannelBt.opened == false) {
                active = false
            }
        }

        //source: "qrc:/qml/desktop/addChannel.qml"
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:720;width:640}D{i:3;anchors_height:480;anchors_x:0;anchors_y:0}
}
 ##^##*/
