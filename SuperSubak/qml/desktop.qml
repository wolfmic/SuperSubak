import QtQuick 2.0
import QtQuick.Window 2.2

Window {
    id: rootWindow
    title: qsTr("SuperSubak")
    //color: "transparent"
    //opacity: 0.4
    width: 920
    height: 450
    //flags: Qt.FramelessWindowHint
    //flags: Qt.WindowSystemMenuHint
    visible: true
    //onActiveFocusItemChanged: print("activeFocusItem", activeFocusItem)

    property bool ignoreCheck: false

    //MouseArea {
    //    anchors.fill: parent
    //    onPressed: {
    //        cursorManager.set(Qt.point(mouseX, mouseY))
    //    }
    //    onMouseXChanged: {
    //        cursorManager.updateX()
    //    }
    //    onMouseYChanged: {
    //        cursorManager.updateY()
    //    }
    //}

    Loader {
        id: chatScreen
        anchors.fill: parent
        source: "desktop/chat.qml";
        visible: true
        //active:true
        //property int r: 1
        //onLoaded: {
        //    // login case
        //    if (r > 0) {
        //        --r;
        //        console.log("------------------ active fDelayed");
        //    } else {
        //        console.log("------------------ active false");
        //        active = false;
        //    }
        //}
    }

    Loader {
        id: loginScreen
        anchors.fill: parent
        source: "desktop/login.qml";
        visible: true
        //width: 250
        //height: 150

        //active:true
        //property int r: 1
        //onLoaded: {
        //    // login case
        //    if (r > 0) {
        //        --r;
        //        console.log("------------------ active fDelayed");
        //    } else {
        //        console.log("------------------ active false");
        //        active = false;
        //    }
        //}
    }

    Connections {
        target: rcAPI
        onLogged: {
            //pageLoader.active = true
            //console.log("------------------ active true")
            //pageLoader.source = "desktop/chat.qml"
            loginScreen.visible = false;
            rcAPI.listAllDMChannels();
            rcAPI.listChannels()
        }
    }

    Connections {
        target: TrayManager

        onSignalShow: {
            rootWindow.show();
        }
        onSignalQuit: {
            ignoreCheck = true;
            close()
        }
        onSignalIconActivated: {
            if(rootWindow.visibility === Window.Hidden) {
                 rootWindow.show()
             } else {
                 rootWindow.hide()
             }
        }
    }

    onClosing: {
        if (ignoreCheck === false) {
            close.accepted = false
            rootWindow.hide()
        } else {
            Qt.quit()
        }
    }

    Rectangle {
        id: errorNotification
        width: errorText.paintedWidth + 20
        height: 60
        color: "#aa772626"
        radius: 5
        property string lastError;
        property string lastCall;
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: -60
        }
        Text {
            id: errorText
            color: "#9da097"
            text: "Network Failure: \"" + errorNotification.lastError + "\" on RestCall: \"" + errorNotification.lastCall + "\"";
            font.family: "Verdana"
            font.pointSize: 12
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                bottomMargin: 10
            }
        }
        SequentialAnimation {
            id: animationPopup
            running: false
            NumberAnimation {
                target: errorNotification
                property: "anchors.topMargin"
                duration: 200
                easing.type: Easing.InOutQuad
                from: -60
                to: -20
            }
            NumberAnimation {
                target: errorNotification
                property: "anchors.topMargin"
                duration: 1600
                easing.type: Easing.InOutQuad
                from: -20
                to: -20
            }
            NumberAnimation {
                target: errorNotification
                property: "anchors.topMargin"
                duration: 200
                easing.type: Easing.InOutQuad
                from: -20
                to: -60
            }
        }

        //Behavior on anchors.topMargin {
        //    NumberAnimation {
        //        duration: 200
        //        easing.type: Easing.InOutQuad
        //    }
        //}

        Connections {
            target: rcAPI
            onNetworkError: {
                // void networkError(int errCode, QString call, QString errString);
                console.log("network Error handled QML: " + errorNotification.lastError + " : " + errorNotification.lastCall);
                errorNotification.lastError = errString;
                errorNotification.lastCall = call;
                errorNotification.anchors.topMargin = 0;
                animationPopup.running = true;
            }
        }

    }
}
