import QtQuick 2.2
import Sailfish.Silica 1.0
import QtQuick.Layouts 1.0

Page {
    id: chatScreen
    anchors.fill: parent

    Item {
        id: chatLayout
        anchors {
            left: parent.left
            leftMargin: 0
            right: parent.right
            rightMargin: 0
            top: parent.top
            topMargin: 0
            bottom: parent.bottom
            bottomMargin: 0
        }
        Rectangle {
            id: loadMoreFeedBack
            width: 250
            height: 60
            color: "#a9919191"
            radius: 15
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: -30
            }
            Behavior on anchors.topMargin {
                NumberAnimation {
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
        }

        ListView {
            id: messageView
            clip: true
            onIsAtBoundaryChanged: {
                if (atYBeginning == true) {
                    loadMoreFeedBack.anchors.topMargin = -(loadMoreFeedBack.height / 2);
                    rcAPI.loadMore();
                } else {
                    loadMoreFeedBack.anchors.topMargin = -(loadMoreFeedBack.height);
                }
            }
            anchors {
                bottom: chatZone.top
                top: parent.top
                left: parent.left
                right: parent.right
            }
            verticalLayoutDirection: ListView.BottomToTop
            snapMode: ListView.NoSnap
            orientation: ListView.Vertical
            model: MessageListModel
            delegate: Item {
                width: messageView.width
                height: (textMessage.paintedHeight < 120) ? 120 : (textMessage.paintedHeight + 60)
                Item {
                    anchors {
                        fill: parent
                    }
                    //Rectangle {
                    //    id: debugRectangleRowSize
                    //    anchors {
                    //        fill: parent
                    //    }
                    //    radius: 10
                    //    color: "red"
                    //}

                    BusyIndicator {
                        id: loadPlaceHolder
                        size: BusyIndicatorSize.Small
                        anchors {
                            left: parent.left
                            leftMargin: 10
                            top: parent.top
                            bottomMargin: 15
                        }
                        running: avatarImage.status != Image.Ready
                    }
                    Image {
                        id: avatarImage
                        width: 60
                        height: 60
                        asynchronous: true
                        sourceSize.width: 32
                        sourceSize.height: 32
                        source: "file://tmp/SuperSubakCache/" + model._id
                        //visible: avatarImage.status == Image.Error
                        anchors {
                            left: parent.left
                            leftMargin: 10
                            top: parent.top
                            bottomMargin: 15
                        }
                    }
                    Text {
                        id: authorName
                        text: model.author
                        color: "#7c628c"
                        anchors {
                            left: parent.left
                            leftMargin: 70
                            top: parent.top
                            bottomMargin: 15
                        }
                        font.bold: true
                    }
                    Text {
                        id: textMessage
                        color: "#decfcf"
                        text: model.message
                        fontSizeMode: Text.VerticalFit
                        font.pointSize: 22
                        textFormat: Text.RichText
                        wrapMode: Text.Wrap
                        //width: parent.width
                        anchors {
                            left: parent.left
                            leftMargin: 70
                            top: authorName.bottom
                            //topMargin: 5
                            right: parent.right
                            rightMargin: 10
                            //bottom: parent.bottom
                        }
                        onLinkActivated: {
                            if (!Qt.openUrlExternally(link)) {
                                rcAPI.fallbackOpenLink(link)
                            }
                        }
                        linkColor: "#0085b7"
                    }
                }
            }

            Connections {
                target: rcAPI
                onRecievedMessage: {
                    loadingIndicator.running = false
                    // textMessage.paintedWidth for multiline
                    MessageListModel.addMessage(messageId, messageString, authorName, userid, attachmentType, oldMode)
                    gc()
                }
            }

            BusyIndicator {
                id: loadingIndicator
                size: BusyIndicatorSize.Medium
                anchors {
                    left: parent.left
                    bottom: parent.bottom
                }
                running: !MessageListModel.rowCount()
                //running: true
            }
        }

        Rectangle {
            id: chatZone
            //height: 60
            height: (chatBox.paintedHeight < 80) ? 80 : (chatBox.paintedHeight + 80)
            color: "#7a424242"
            anchors {
                left: parent.left
                leftMargin: 0
                right: parent.right
                rightMargin: 0
                bottom: parent.bottom
                bottomMargin: 0
            }

            TextEdit {
                id: chatBox
                color: "#ededed"
                font.pointSize:22
                property string placeholderText: "Hi !"
                Text {
                    text: parent.placeholderText
                    font.pointSize: parent.font.pointSize
                    lineHeight: 0.7
                    color: "#4cffffff"
                    font.pixelSize: parent.font.pixelSize
                    visible: !parent.text
                }
                wrapMode: Text.WordWrap
                anchors {
                    rightMargin: 10
                    leftMargin: 10
                    bottomMargin: 10
                    topMargin: 10
                    fill: parent
                }
                horizontalAlignment: Text.AlignLeft
                font.family: "Verdana"
                textFormat: Text.AutoText
                transformOrigin: Item.Bottom
                selectByMouse: true
                focus: true
                Keys.onReturnPressed: {
                    rcAPI.postmessage(chatBox.text);
                    chatBox.text="";
                }
            }
        }
    }

    //pushAttached(var page, object properties)
    //    Button {
    //        text: "Next page"
    //        anchors.horizontalCenter: parent.horizontalCenter
    //        onClicked: pageStack.push(Qt.resolvedUrl("login.qml"))
    //    }

}

/*##^## Designer {
    D{i:3;anchors_height:480;anchors_x:0;anchors_y:0}
}
 ##^##*/
