import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.Layouts 1.0

Page {
    ListView {
        id: channelListView
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        x: 15
        clip: true
        anchors {
            right: parent.right
            left: parent.left
            //top: parent.top
            topMargin: -15
            top: logo.bottom
            bottomMargin: 15
            bottom: addChannelBt.top
        }
        onCurrentItemChanged: {
            MessageListModel.clear();
            rcAPI.selectChannel(model.getId(currentIndex), model.getName(currentIndex), model.getType(currentIndex))
        }
        highlight: Rectangle {
            height: 150;
            width: channelListView.width;
            color: "#995d5d5d"
            y: channelListView.currentItem.y
            Behavior on y { SmoothedAnimation { velocity: 700 } }
        }
        highlightFollowsCurrentItem: false
        delegate: ListItem {
              id: background
              height: 150
              //radius: 10

              //height: 150
              //width: channelListView.width

              //color: "#6b6b6b"
              //color: mouseArea.containsMouse ? "#5f6b6b6b" : "#0f6b6b6b"
              //anchors.left: parent.left
              anchors.fill: parent.fill
              Rectangle {
                  id: channelIcon
                  width: 120
                  height: 120
                  //radius: 10
                  color: colorCode
                  opacity: 0.7
                  anchors.verticalCenter: parent.verticalCenter
                  Text {
                      color: "#ffffff"
                      text: model.name[0]
                      font.capitalization: Font.AllUppercase
                      font.pointSize: 72
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      anchors.fill: parent
                  }
              }
              Text {
                  id: chanName
                  text: model.name
                  clip: true
                  anchors.verticalCenter: parent.verticalCenter
                  anchors.left: channelIcon.right
                  anchors.leftMargin: 15
                  font.pointSize: 52
                  color: "#9da097"
              }
              MouseArea {
                  id: mouseArea
                  anchors.fill: parent
                  onClicked: {
                      channelListView.currentIndex = index;
                      MessageListModel.clear();
                      rcAPI.selectChannel(model._id, model.name, model.type);
                  }
              }
        }
        model: ChannelListModel

        Connections {
            target: rcAPI
            onAddJoinedChannel: {
                console.log(channelName + " " + type)
               ChannelListModel.addChannel(channelId, channelName, lastUpdated, type);
            }
            onUpdateChannel: {
               ChannelListModel.setChannel(channelId, notif, update);
            }
        }
    }
}
