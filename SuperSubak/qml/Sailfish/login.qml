import QtQuick 2.2
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

Page {
    id: loginScreen
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: flickable
        anchors.fill: parent
        PullDownMenu {
            MenuItem {
                text: "Connect"
                onClicked: {
                    rcAPI.login(loginInput.text, passInput.text, serverInput.text)
                }
            }
        }

        Connections {
            target: rcAPI
            onLogged: {
                pageStack.clear()
                pageStack.push(Qt.resolvedUrl("chat.qml"))
                pageStack.pushAttached(Qt.resolvedUrl("channel.qml"))
                rcAPI.listAllDMChannels();
                rcAPI.listChannels()
           }
        }

        Rectangle {
            id: server
            height: 50
            color: "#45ffffff"
            radius: 10
            anchors.left: parent.left
            anchors.leftMargin: 80
            anchors.right: parent.right
            anchors.rightMargin: 80
            anchors.verticalCenterOffset: -170
            anchors.verticalCenter: parent.verticalCenter
            TextInput {
                id: serverInput
                height: server.height
                color: "#ffffff"
                text: qsTr("rc.subak.ovh")
                anchors.left: parent.left
                anchors.leftMargin: 30
                anchors.right: parent.right
                anchors.rightMargin: 30
                anchors.verticalCenter: parent.verticalCenter
                selectionColor: "#803500"
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 20
                selectByMouse: true
            }
        }

        Rectangle {
            id: login
            height: 80
            color: "#45ffffff"
            radius: 10
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.verticalCenterOffset: -60
            anchors.verticalCenter: parent.verticalCenter
            TextInput {
                id: loginInput
                height: 44
                color: "#ffffff"
               property string placeholderText: "login"
               Text {
                   text: parent.placeholderText
                   anchors.fill: parent
                   lineHeight: 0.7
                   color: "#60ffffff"
                   font.pixelSize: parent.font.pixelSize
                   visible: !parent.text
               }
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.verticalCenter: parent.verticalCenter
                selectionColor: "#803500"
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 30
                selectByMouse: true

                Keys.onReturnPressed: {
                    rcAPI.login(loginInput.text, passInput.text, serverInput.text)
                }
            }
        }

        Rectangle {
            id: pass
            height: 80
            color: "#45ffffff"
            radius: 10
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.verticalCenterOffset: 60
            anchors.verticalCenter: parent.verticalCenter
            TextInput {
                id: passInput
                height: 44
                color: "#ffffff"
                property string placeholderText: "password"
                Text {
                    text: parent.placeholderText
                    anchors.fill: parent
                    lineHeight: 0.7
                    color: "#60ffffff"
                    font.pixelSize: parent.font.pixelSize
                    visible: !parent.text
                }
                font.family: "Verdana"
                echoMode: TextInput.Password
                horizontalAlignment: Text.AlignLeft
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 30
                selectByMouse: true
            }
            Keys.onReturnPressed: {
                rcAPI.login(loginInput.text, passInput.text, serverInput.text)
            }
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:11;anchors_height:100;anchors_width:100;anchors_x:"-15";anchors_y:54}
}
 ##^##*/
