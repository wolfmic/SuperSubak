import QtQuick 2.0
import Sailfish.Silica 1.0
import "Sailfish"

ApplicationWindow {
    initialPage: Qt.resolvedUrl("Sailfish/login.qml")
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations


    Loader {
        source: "Sailfish/errorNotifs.qml"
    }
}
