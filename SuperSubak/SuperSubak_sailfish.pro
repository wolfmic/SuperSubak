PKGCONFIG += sailfishapp

TARGET = SuperSubak # The name of your application (SAILFISHOS)

QT += quick network
CONFIG += c++11 qtquickcompiler

# static build of QT
#LFLAGS = -static
#QMAKE_CFLAGS_RELEASE+= -Os -momit-leaf-frame-pointer
#QMAKE_LFLAGS+= -static -static-libgcc
#DEFINES+= QT_STATIC_BUILD
DEFINES+= SFOS

CONFIG += sailfishapp # (SAILFISHOS)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += src/main.cpp \
    src/cursorManager.cpp \
    src/rocketchat.cpp \
    src/channellistmodel.cpp \
    src/messagelistmodel.cpp

RESOURCES += qml_sailfish.qrc #look like sailfish doen't like that (SAILFISHOS)

# Additional import path used to resolve QML modules in Qt Creator's code model
# QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
# QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#LIBS+= -LC:\OpenSSL-Win32\bin -leay32 -lssleay32 #looks highly windows specific

# (SAILFISHOS)
DISTFILES +=  qml/SuperSubak.qml \
    qml/cover/CoverPage.qml \
    qml/Sailfish/chat.qml \
    qml/Sailfish/loading.qml \
    qml/Sailfish/login.qml \
    qml/Sailfish/errorNotifs.qml \
    rpm/SuperSubak.changes.in \
    rpm/SuperSubak.changes.run.in \
    rpm/SuperSubak.spec \
    rpm/SuperSubak.yaml \
    translations/*.ts \
    SuperSubak.desktop \
    logo.png

HEADERS += src/cursorManager.h \
    src/rocketchat.h \
    src/channellistmodel.h \
    src/messagelistmodel.h \
    src/user.h

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172
