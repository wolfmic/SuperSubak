TARGET = SuperSubak

QT += quick network opengl
CONFIG += c++11 qtquickcompiler

# static build of QT
#LFLAGS = -static
#QMAKE_CFLAGS_RELEASE+= -Os -momit-leaf-frame-pointer
#QMAKE_LFLAGS+= -static -static-libgcc
#DEFINES+= QT_STATIC_BUILD


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# Comment this part if you are not targeting windows
DEFINES += WIN

# Comment this part if you are not targeting unix
# DEFINES += UNI


SOURCES += src/main.cpp \
    src/cursorManager.cpp \
    src/rocketchat.cpp \
    src/channellistmodel.cpp \
    src/messagelistmodel.cpp \
    src/serverListModel.cpp \
    src/userListModel.cpp \
    src/trayManager.cpp

RESOURCES += qml.qrc
#QMAKE_CXX=clang++

# Additional import path used to resolve QML modules in Qt Creator code model
# QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
# QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS+= #-LC:\OpenSSL-Win32\bin -leay32 -lssleay32 #looks highly windows specific

DISTFILES += logo.png \
    watermelon-wallpaper1.jpg \
    icons\86x86\SuperSubak.png

DISTFILES +=  qml/desktop.qml \
    qml/desktop/chat.qml \
    qml/desktop/loading.qml \
    qml/desktop/login.qml \

HEADERS += src/cursorManager.h \
    src/rocketchat.h \
    src/user.h \
    src/channellistmodel.h \
    src/messagelistmodel.h \
    src/serverListModel.h \
    src/userListModel.h \
    src/trayManager.h
