#ifndef CHANNELLISTMODEL_H
#define CHANNELLISTMODEL_H

#include "enableDebugDraw.h"

#include <QObject>
#include <QDateTime>
#include <QAbstractListModel>
#include <QColor>

class ChannelListModel : public QAbstractListModel {
    Q_OBJECT
public:
    struct ChannelData {
        QString _id;
        QString name;
        QColor colorCode;
        QDateTime lastUpdate;
        int notif;
        int type;
    };
    enum Role {
        ID,
        NAME,
        COLORCODE,
        LASTUPDATE,
        NOTIFICATION,
        TYPE
    };
private:
    QList<ChannelData> channels;
    float currentHue;
public:
    explicit ChannelListModel(QObject* parent = Q_NULLPTR);
    virtual ~ChannelListModel() override {}
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    Q_SLOT void addChannel(QString id, QString name, QDateTime lastUpdate, /*RocketChat::ChannelType*/ int type);
    Q_SLOT void setChannel(QString id, int notif, QDateTime lastUpdate);
    Q_INVOKABLE QString getName(int);
    Q_INVOKABLE void clear();
    Q_INVOKABLE QString getId(int);
    Q_INVOKABLE int getType(int index);
};

#endif // CHANNELLISTMODEL_H
