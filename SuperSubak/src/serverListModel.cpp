#include "serverListModel.h"

ServerListModel::ServerListModel(QObject* parent) : QAbstractListModel(parent), currentHue(0.0f) {
    //_servers.push_back({"id", "name0", "token", "blue", QDateTime(), 0, 0});
    //_servers.push_back({"id", "name1", "token", "blue", QDateTime(), 0, 0});
    //_servers.push_back({"id", "name2", "token", "blue", QDateTime(), 0, 0});
    //_servers.push_back({"id", "name3", "token", "blue", QDateTime(), 0, 0});
    //_servers.push_back({"id", "name4", "token", "blue", QDateTime(), 0, 0});
}

QHash<int, QByteArray> ServerListModel::roleNames() const {
    QHash<int,QByteArray> rez;
    rez[ID]="_id";
    rez[NAME]="name";
    rez[COLORCODE]="colorCode";
    rez[LASTUPDATE]="lastUpdate";
    rez[AUTHTOKEN]="authTocken";
    rez[NOTIFICATION]="notificationAmount";
    rez[TYPE]="type";
    return rez;
}
int ServerListModel::rowCount(const QModelIndex &) const {
    return _servers.size();
}
QVariant ServerListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= _servers.size())
    {
        return QVariant("invalid id");
    }
    const ServerData& entry = _servers[index.row()];
    if (role == ID) {
        return QVariant(entry._id);
    } else if (role == NAME){
        return QVariant(entry.name);
    } else if (role == COLORCODE) {
        return QVariant::fromValue(entry.colorCode);
    } else if (role == LASTUPDATE) {
        return QVariant::fromValue(entry.lastUpdate.toString(Qt::ISODate));
    } else if (role == NOTIFICATION) {
        return QVariant::fromValue(entry.notif);
    } else if (role == AUTHTOKEN) {
        return QVariant::fromValue(entry.authToken);
    } else if (role == TYPE) {
        return QVariant::fromValue(entry.type);
    }
    
    
    return QVariant("invalid member name");
}

void ServerListModel::clear() {
    beginRemoveRows(QModelIndex(), 0, _servers.size());
    _servers.clear();
    endRemoveRows();
}

void ServerListModel::addServer(QString id, QString name, QDateTime lastUpdate, QString configFile, int type) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());   // kindly provided by superclass
    //name.replace(0, 1, name[0].toUpper());
    // object creation based on params...
    currentHue += 0.618033988749895f;
    currentHue = currentHue - (int)currentHue * 1.0f;
    //currentHue = std::fmod(currentHue, 1.0f);
    ServerData c{id, name, "", QColor::fromHslF(currentHue, 0.3, 0.3), lastUpdate, configFile, 0, type, RocketChat() };
    _servers << c;
    endInsertRows();
}

// TODO: I should make a Map because of research performances which can be super slow with a lot of channels
void ServerListModel::setServer(QString id, int notif, QDateTime lastUpdate) {
    ServerData* c = nullptr;
    int i = 0;
    for (ServerData& x : _servers) {
        if (x._id == id) {
            c = &x;
            break;
        }
        ++i;
    }
    if (c == nullptr) {
        //qDebug() << "couldn't found channel: " << id;
        return;
    }
    c->notif = notif;
    c->lastUpdate = lastUpdate;
    // TODO dunno the performance impact of this thing
    emit dataChanged(index(i), index(i));
}

QString ServerListModel::getName(int index) {
    return _servers[index].name;
}

QString ServerListModel::getId(int index) {
    return _servers[index]._id;
}

int ServerListModel::getType(int index) {
    return _servers[index].type;
}

void ServerListModel::changeServer(int index) {
    RocketChat& r = (RocketChat&)_servers.at(index).rc;
    r.logFromFile(_servers.at(index).configFilePath);
    _qmlContext->setContextProperty("rcAPI", (RocketChat*)&_servers.at(index).rc); // discarding const operator as it create problems in qml
}
