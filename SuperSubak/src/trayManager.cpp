#include "trayManager.h"

TrayManager::TrayManager(QObject *parent) : QObject(parent) {
    QMenu* trayIconMenu = new QMenu();
    QAction* viewWindow = new QAction("Show", this);
    QAction* quitAction = new QAction("Quit", this);

    connect(viewWindow, &QAction::triggered, this, &TrayManager::signalShow);
    connect(quitAction, &QAction::triggered, this, &TrayManager::signalQuit);

    trayIconMenu->addAction(viewWindow);
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon();
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon("://icons/86x86/SuperSubak.png"));
    trayIcon->show();
    trayIcon->setToolTip("Show Super Subak");

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
        this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}

void TrayManager::iconActivated(QSystemTrayIcon::ActivationReason reason) {
    switch (reason) {
        case QSystemTrayIcon::Trigger:
            emit signalIconActivated();
            break;
        default:
            break;
    }
}

void TrayManager::hideIconTray() {
    trayIcon->hide();
}
