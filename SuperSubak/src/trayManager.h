#include <QSystemTrayIcon>
#include <QQuickWindow>
#include <QObject>
#include <QMenu>
#include <QAction>

class TrayManager : public QObject {
    Q_OBJECT

public:
    explicit TrayManager(QObject *parent = nullptr);
    virtual ~TrayManager() = default;

signals:
    void signalIconActivated();
    void signalShow();
    void signalQuit();

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);

public slots:
    void hideIconTray();

private:
    QSystemTrayIcon *trayIcon;
};
