#include "enableDebugDraw.h"
#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include "serverListModel.h"
#include "channellistmodel.h"
#include "messagelistmodel.h"
#include "userListModel.h"
#include "cursorManager.h"
#include "rocketchat.h"
#include "trayManager.h"

#ifdef SFOS

#include <sailfishapp.h>

int main(int argc, char *argv[]) {
    QGuiApplication* app = SailfishApp::application(argc, argv);
    QQuickView* view = SailfishApp::createView();
    RocketChat rc;
    view->rootContext()->setContextProperty("rcAPI", &rc);
    ChannelListModel chan;
    view->rootContext()->setContextProperty("ChannelListModel", &chan);
    MessageListModel mess;
    view->rootContext()->setContextProperty("MessageListModel", &mess);
    UserListModel ul;
    view->rootContext()->setContextProperty("UserListModel", &ul);
    QObject::connect(&rc, &RocketChat::registerUser, &ul, &UserListModel::addUser);
    
    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    QFileInfo check_file("/tmp/SuperSubakCache/authToken");
    if (check_file.exists()) {
        rc.logFromFile("/tmp/SuperSubakCache/authToken");
    } else {
        qDebug() << "no local token found";
    }
    return app->exec();
}

#else

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    app.setAttribute(Qt::AA_EnableHighDpiScaling);
    app.setAttribute(Qt::AA_UseDesktopOpenGL);
    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);
    fmt.setVersion(3, 2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(fmt);
    QQmlApplicationEngine engine;

    //CursorManager cm;
    //engine.rootContext()->setContextProperty("cursorManager", &cm);
    RocketChat rc;
    //engine.rootContext()->setContextProperty("rcAPI", &rc);
    ChannelListModel chan;
    engine.rootContext()->setContextProperty("ChannelListModel", &chan);
    ServerListModel serv;
    serv._qmlContext = engine.rootContext();
    engine.rootContext()->setContextProperty("ServerListModel", &serv);
    ChannelListModel chan2;
    engine.rootContext()->setContextProperty("AllChannelListModel", &chan2);
    MessageListModel mes;
    engine.rootContext()->setContextProperty("MessageListModel", &mes);
    UserListModel ul;
    engine.rootContext()->setContextProperty("UserListModel", &ul);
    TrayManager tm;
    engine.rootContext()->setContextProperty("TrayManager", &tm);


    //QFileInfo check_file("/tmp/SuperSubakCache/authToken");
    qDebug() << "config folder: " << RocketChat::getConfigFolder();
    qDebug() << "cache folder: " << RocketChat::getCacheFolder();
    QDir configDir(RocketChat::getConfigFolder());
    for (QFileInfo& f : configDir.entryInfoList()) {
        //QFileInfo check_file(rc.getCacheFolder() + "/authToken");
        if (f.suffix() == "authToken") {
            serv.addServer("id", f.completeBaseName(), QDateTime(), f.absoluteFilePath(), 0);
            qDebug() << "----- adding server" << f.completeBaseName();
        }
    }
    if (serv._servers.size()) {
        serv.changeServer(0);
        QObject::connect(&serv._servers.at(0).rc, &RocketChat::registerUser, &ul, &UserListModel::addUser);
        qDebug() << "server set";
    }
    engine.load(QUrl(QStringLiteral("qrc:/qml/desktop.qml")));
    RocketChat defaultRc;
    if (serv._servers.size()) {
        ((RocketChat&)serv._servers.at(0).rc).logFromFile(serv._servers.at(0).configFilePath);
    } else {
        engine.rootContext()->setContextProperty("rcAPI", &defaultRc);
    }
    if (engine.rootObjects().isEmpty())
        return -1;
    QQuickWindow* w = qobject_cast<QQuickWindow*>(engine.rootObjects().first());
    if (!w) {
        return -1;
    }
    w->setColor(QColor(Qt::transparent));
    w->setClearBeforeRendering(true);
    //cm.mainView = w;

    QRect screenGeometry = QGuiApplication::primaryScreen()->geometry();
    w->setPosition(QPoint((screenGeometry.width() - w->width()) / 2, (screenGeometry.height() - w->height()) / 2));

    return app.exec();
}
#endif
