#ifndef userListModel_h
#define userListModel_h

#include "enableDebugDraw.h"

#include <QObject>
#include <QDateTime>
#include <QAbstractListModel>
#include <QColor>

class UserListModel : public QAbstractListModel {
    Q_OBJECT
    public:
    struct UserData {
        QString _id;
        QString name;
        bool operator==(const UserData&)const;
    };
    enum Role {
        ID,
        NAME
    };
    private:
    QList<UserData> users;
    float currentHue;
    public:
    explicit UserListModel(QObject* parent = Q_NULLPTR);
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    Q_SLOT void addUser(QString id, QString name);
    Q_SLOT void setUser(QString name);
    Q_INVOKABLE QString getName(int);
    Q_INVOKABLE void clear();
    Q_INVOKABLE QString getId(int);
};

#endif
