#include "rocketchat.h"

#ifdef UNI
#include <unistd.h>
#elif WIN
#include <windows.h>
#endif

#include <QDateTime>
#include <QDebug>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QStandardPaths>

RocketChat::RocketChat(QObject *parent) : QObject(parent), _qnam(this), getMessagesTimer(this) {
    _selectedChannel.chanName = "#general";
    _selectedChannel.type = ChannelType::Channel;
    connect(&getMessagesTimer, SIGNAL(timeout()), this, SLOT(checkNewMessages()));
    QDir cacheDir;
    //cacheDir.mkdir("/tmp/SuperSubakCache/");
    cacheDir.mkdir(getConfigFolder());
    cacheDir.mkdir(getCacheFolder());
    qDebug() << "init";
}

RocketChat::RocketChat(const RocketChat& o) : QObject(o.parent()), _qnam(this), getMessagesTimer(this) {
    qDebug() << "copy";
    _selectedChannel.chanName = "#general";
    _selectedChannel.type = ChannelType::Channel;
    connect(&getMessagesTimer, SIGNAL(timeout()), this, SLOT(checkNewMessages()));
}

RocketChat& RocketChat::operator=(const RocketChat&) {
    qDebug() << "operator";
    return *this;
}

void RocketChat::fallbackOpenLink(QUrl url) {
    static QProcess firefox; // temporary fallback property but it would a good idea to have a link to a browser
    //firefox.startDetached("open", QStringList() << url.toString());
    //return;
    if (firefox.startDetached("firefox-bin", QStringList() << url.toString()) != 0){
        qDebug() << "trying to run firefox...";
    } else if (firefox.startDetached("firefox", QStringList() << url.toString()) != 0){
        qDebug() << "trying to run firefox...";
    } else if (firefox.startDetached("chrome", QStringList() << url.toString()) != 0){
        qDebug() << "trying to run chrome...";
    } else if (firefox.startDetached("chromium", QStringList() << url.toString()) != 0){
        qDebug() << "trying to run chromium...";
    } else {
        qDebug() << "could not find a working browser :(";
    }
}

QString RocketChat::getCacheFolder() {
    return QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/SuperSubakCache";
}

QString RocketChat::getConfigFolder() {
    return QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/SuperSubak";
}

void RocketChat::checkNewMessages() {
    //qDebug() << "check messages";
    if (_selectedChannel.lastMessageTimestamp.size() != 0) {
        getChannelMessages(_selectedChannel.chanId, GetMessageMode::Latest, _selectedChannel.lastMessageTimestamp, _selectedChannel.type);
        getSubscriptions();
    } else {
        qDebug() << "timeStamp is empty";
    }
}

QNetworkRequest RocketChat::getAuthNetworkRequest(QString path, QUrl postUrl) {
    //QUrl postUrl = _serverUrl;
    postUrl.setPath(path);
    QNetworkRequest request(postUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader(QByteArray("X-Auth-Token"), QByteArray(_me.authToken.toUtf8()));
    request.setRawHeader(QByteArray("X-User-Id"), QByteArray(_me.userId.toUtf8()));

    //qDebug() << "request:" << postUrl;
    //qDebug() << "authToken" << _me.authToken;
    //qDebug() << "userId" <<  _me.userId;
    //qDebug() << "me" << _me.username;
    return request;
}

void RocketChat::connectErrorCallback(QNetworkReply* reply) {
     connect(reply,  static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, [this, reply](QNetworkReply::NetworkError err) {

          if (err != QNetworkReply::NoError) {
              qDebug() << "error when trying to request REST API: " << err;
              emit networkError(err, reply->url().toString(), reply->errorString());
          }
     });
}

void RocketChat::serverInfos() {
    QUrl getUrl = _serverUrl;
    getUrl.setPath("/api/v1/info");
     if (!getUrl.isValid()) {
         qDebug() << "URL invalid: " << getUrl << "\n";
         return;
     }
     qDebug() << "requestURL: " << getUrl << "\n";

     QNetworkRequest restCall(getUrl);
     QNetworkReply *reply = _qnam.get(restCall);
     connectErrorCallback(reply);
     connect(reply, &QNetworkReply::finished, this, [reply](){
         reply->deleteLater();
         //qDebug() << "serverInfo: " << reply->readAll() << "\n";
         const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
         const QJsonObject obj = doc.object();

         if (obj.value("success").toString() == "error") {
             qWarning() << "error: " << obj.value("error").toInt() << "\n";
         } else {
             qDebug() << "info: " << obj.value("info") << "\n";
         }
     });

}

void RocketChat::logFromFile(QString file) {
    QFile authTokenFile;
    authTokenFile.setFileName(file);

    authTokenFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QString fileContent = authTokenFile.readAll();
    QJsonDocument d = QJsonDocument::fromJson(fileContent.toUtf8());
    QJsonObject data(d.object());
    _me.authToken = data["authToken"].toString();
    _me.userId = data["userId"].toString();
    _me.username = data["me"].toObject()["name"].toString();
    _serverUrl.setScheme(data["serverScheme"].toString());
    _serverUrl.setHost(data["serverHost"].toString());
    qDebug() << "login from file";
    qDebug() << "authToken" << _me.authToken;
    qDebug() << "userId" <<  _me.userId;
    qDebug() << "me" << _me.username;
    qDebug() << "server" << _serverUrl;

    //qDebug() << "authToken" << data["authToken"].toString();
    //qDebug() << "userId" << data["userId"].toString();
    //qDebug() << "me" << data["name"].toString();
    emit logged();
    getMessagesTimer.start(3000);
}

void RocketChat::login(QString login, QString password, QString server) {
    _serverUrl.setScheme("https");
    _serverUrl.setHost(server);

    qDebug() << "--rcAPI--: trying to log: " << login << " pass: " << password << " Server: " << server << "\n";
    QUrl postUrl = _serverUrl;
    postUrl.setPath("/api/v1/login");
     if (!postUrl.isValid()) {
         qDebug() << "URL invalid: " << postUrl << "\n";
     }
     QJsonObject postData {
         {"user", login },
         {"password", password }
     };
     QNetworkRequest restCall(postUrl);
     restCall.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
     qDebug() << "requestURL: " << postUrl << "\n";
     QNetworkReply *reply = _qnam.post(restCall, QJsonDocument(postData).toJson(QJsonDocument::Compact));
     connectErrorCallback(reply);
     connect(reply, &QNetworkReply::finished, this, [this, reply](){
         reply->deleteLater();
         const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
         const QJsonObject obj(doc.object());

         if (obj.value("status").toString() == "success") {
             qDebug() << "login status: " << obj.value("status").toString() << "\n";
             QJsonObject data(obj["data"].toObject());
             _me.authToken = data["authToken"].toString();
             _me.userId = data["userId"].toString();
             _me.username = data["me"].toObject()["name"].toString();
             qDebug() << "authToken: " << _me.authToken << " userID: " << _me.userId;
             // should send the signed "logged" to the main QML now...
             emit logged();
             getMessagesTimer.start(3000);
             QFile authTokenFile;
             //authTokenFile.setFileName("/tmp/SuperSubakCache/authToken");
             authTokenFile.setFileName(getConfigFolder() + "/" + _serverUrl.host() + ".authToken");
             authTokenFile.open(QIODevice::WriteOnly);

             data["serverScheme"] = _serverUrl.scheme();
             data["serverHost"] = _serverUrl.host();
             qDebug() << "object modified: " << data << "\n\n";
             //authTokenFile.write(doc.toJson());

             //QJsonObject serverData {
             //    //{"channel", "#rcdev" },
             //    {"serverScheme", _serverUrl.scheme() },
             //        {"serverHost", _serverUrl.host() }
             //};
             QJsonDocument serverDatDoc(data);
             authTokenFile.write(serverDatDoc.toJson());
         } else {
             qWarning() << "login error: " << obj.value("error").toInt() << "\n";
             emit loginFailed();
         }
     });
}

void RocketChat::selectChannel(QString channelId, QString channelName, int type) {
    qDebug() << "selected channel: " << channelName << "\n";
    _selectedChannel.chanName = QString("#") + channelName;
    _selectedChannel.chanId = channelId;
    _selectedChannel.type = (RocketChat::ChannelType)type;
    //lastMessageTimestamp = "";
    emit listChannelMembers(_selectedChannel.chanId);
    emit getChannelMessages(_selectedChannel.chanId, GetMessageMode::Default, "", _selectedChannel.type);
}

QString RocketChat::selectedChannel() {
    return _selectedChannel.chanName;
}

void RocketChat::postmessage(QString message){
    qDebug() << "--rcAPI--: msg: " << message << "\n";
    //static bool previousSend = true;
    //if (previousSend == false) {
    //    qDebug() << "--rcAPI--: msg: waiting for the previous to be send\n";
    //    return;
    //}
    //previousSend = false;
    if (message.size() == 0) {
        return;
    }
    QJsonObject postData {
        //{"channel", "#rcdev" },
        {"roomId", _selectedChannel.chanId },
        {"text", message }
    };

    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/chat.postMessage", _serverUrl));
    QNetworkReply *reply = _qnam.post(restCall, QJsonDocument(postData).toJson(QJsonDocument::Compact));
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [reply]() {
        //previousSend = true;
        reply->deleteLater();
        qDebug() << "PostMessage\n";
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj(doc.object());
        qDebug() << obj;
    });
}

void RocketChat::listChannels(){
    qDebug() << "--rcAPI--: getting channel\n";

    qDebug() << "list channel server: " << _serverUrl;
    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/channels.list.joined", _serverUrl));

    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply](){
        reply->deleteLater();
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj = doc.object();

        qDebug() << "getting channel: " <<  obj;
        if (obj.value("success").toString() == "error") {
            qWarning() << "error: " << obj.value("error").toInt() << "\n";
        } else {
            //qDebug() << "info: " << obj.value("info") << "\n";
            for (const QJsonValue& jvchan: obj["channels"].toArray()) {
                QJsonObject jchan(jvchan.toObject());
                qDebug() << "channelInfo: " << jchan["_id"].toString();
                qDebug() << "channelInfo: " << jchan["name"].toString();
                qDebug() << "channelInfo: " << QDateTime::fromString(jchan["_updatedAt"].toString(), Qt::ISODate);
                qDebug() << "\n";
                emit addJoinedChannel(jchan["_id"].toString(), jchan["fname"].toString(), QDateTime::fromString(jchan["_updatedAt"].toString(), Qt::ISODate), ChannelType::Channel);
            }
        }
    });
}

// From version 0.50.0 and on you can call the methods using dm instead of im.
// https://rocket.chat/docs/developer-guides/rest-api/im/list/
void RocketChat::listAllDMChannels() {
    qDebug() << "--rcAPI--: list DM channels\n";
    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/im.list", _serverUrl));
    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply](){
        reply->deleteLater();
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj = doc.object();

        if (obj.value("success").toString() == "error") {
            qWarning() << "error: " << obj.value("error").toInt() << "\n";
        } else {
            for (const QJsonValue& jvchanDM: obj["ims"].toArray()) {
                QJsonObject jchanDM(jvchanDM.toObject());
                qDebug() << "_id: " << jchanDM["_id"].toString();
                qDebug() << "usernames: " << jchanDM["usernames"].toArray()[1].toString();
                //qDebug() << "topic: " << jchanDM["topic"].toString();
                qDebug() << "\n";
                //qDebug() << jchanDM;
                emit addJoinedChannel(jchanDM["_id"].toString(), jchanDM["usernames"].toArray()[0].toString() + " + " + jchanDM["usernames"].toArray()[1].toString(), QDateTime::fromString(jchanDM["_updatedAt"].toString(), Qt::ISODate), ChannelType::DM);
            }
        }
    });
}

void RocketChat::listChannelMembers(QString chanId) {
    qDebug() << "--rcAPI--: getting members\n";

    QUrlQuery query;
    query.addQueryItem("roomId", chanId.toUtf8());
    QUrl geturl = _serverUrl;
    geturl.setQuery(query);

    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/channels.members", geturl));

    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply](){
        reply->deleteLater();
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj = doc.object();

        if (obj.value("success").toString() == "error") {
            qWarning() << "error: " << obj.value("error").toInt() << "\n";
        } else {
            qDebug() << "info: " << obj.value("count") << "\n";
            for (const QJsonValue& jvchan: obj["members"].toArray()) {
                QJsonObject jchan(jvchan.toObject());
                qDebug() << "userData:" << jchan;
                QString id = jchan["_id"].toString();
                emit registerUser(id, jchan["name"].toString());
                emit getAvatar(id);
            }
        }
        //qDebug() << obj;
    });
}

void RocketChat::getAvatar(QString userId) {
    qDebug() << "--rcAPI--: downloading avatar";

    QUrlQuery query;
    query.addQueryItem("userId", userId);
    QUrl geturl = _serverUrl;
    geturl.setQuery(query);

    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/users.getAvatar", geturl));

    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply, userId]() {
        reply->deleteLater();
        QString link(reply->readAll());
        link.replace(QRegExp("\"(\\S+)\""), "\\1");
        QUrl lk(link);

        //QFileInfo check_file("/tmp/SuperSubakCache/" + userId);
        QFileInfo check_file(getCacheFolder() + "/" + userId);
        if (lk.isValid() && !check_file.exists()) {
            qDebug() << "avatar link:" << link;
            qDebug() << "QUrl avatar link:" << lk;
            emit downloadImage(link, userId);
        }
    });
}

void RocketChat::downloadImage(QUrl url, QString filename) {
    qDebug() << "download link:" << url;
    QNetworkRequest restCall(url);
    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);

    //connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
    //    this, SLOT(updateDownloadProgress(qint64,qint64)));

    connect(reply, &QNetworkReply::downloadProgress,
            this, [](qint64 bytesRead, qint64 totalBytes) {
        qDebug() << "file download progress:" << bytesRead / totalBytes;
    });
    connect(reply, &QNetworkReply::finished, this, [reply, filename](){
        reply->deleteLater();
        QVariant type = reply->header(QNetworkRequest::ContentTypeHeader);
        qDebug() << "type: " << type.toString();
        QFile image;
        //image.setFileName("/tmp/SuperSubakCache/" + filename);
        image.setFileName(getCacheFolder() + "/" + filename);
        image.open(QIODevice::WriteOnly);
        image.write(reply->readAll());
    });
}

void RocketChat::getChannelMessages(QString chanId, GetMessageMode mode, QString timestamp, ChannelType type) {
    // https://rocket.chat/docs/developer-guides/rest-api/channels/history/
    //qDebug() << "--rcAPI--: getMessages of a channel\n";
    static bool replied = true;
    if (replied == false) {
        return;
    }
    QUrl geturl = _serverUrl;
    QUrlQuery query;
    query.addQueryItem("roomId", chanId.toUtf8());
    bool reverseMode = true;
    if (mode == GetMessageMode::Oldest) {
        qDebug() << "++++++ getFromOldest: " << timestamp;
        //query.addQueryItem("oldest", _selectedChannel.lastMessageTimestamp);
        query.addQueryItem("latest", timestamp);
    } else if (mode == GetMessageMode::Latest){
        reverseMode = false;
        qDebug() << "getFromLatest: " << timestamp;
        //query.addQueryItem("oldest", _selectedChannel.lastMessageTimestamp);
        query.addQueryItem("oldest", timestamp);
    }
    query.addQueryItem("count", "100");
    geturl.setQuery(query);

    if (type == ChannelType::Unknown) {
        qWarning() << "channeltype is inknown restCalls will fails";
    }
    QNetworkRequest restCall(getAuthNetworkRequest(type == DM ? "/api/v1/im.history" : "/api/v1/channels.history", geturl));
    //qDebug() << restCall.url();
    const QList<QByteArray>& rawHeaderList(restCall.rawHeaderList());
    for (QByteArray rawHeader: rawHeaderList) {
        //qDebug() << "header:" << rawHeader << "value: " << restCall.rawHeader(rawHeader);
    }

    QNetworkReply *reply = _qnam.get(restCall);
    replied = false;
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply, reverseMode](){
        reply->deleteLater();
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj = doc.object();

        if (obj.value("success").toString() == "error") {
            qWarning() << "error: " << obj.value("error").toInt() << "\n";
        } else {
            QJsonArray jmessageArray(obj["messages"].toArray());
            // we need to  call recievedMessage in the reverse order when getting olderMessages to avoid complex logic in qml and MessageListModel::addMessage
            int i = reverseMode ? (0) : (jmessageArray.size() - 1);
            while (reverseMode ? (i != jmessageArray.size()) : (i >= 0)) {
                 const QJsonValue& m = jmessageArray[i];
                 QJsonObject jmess(m.toObject());
                 //qDebug() << "message: " << jmess["msg"].toString() << "\n";
                 QJsonObject juser(jmess["u"].toObject());
                 //struct User {
                 //    QString authToken;
                 //    QString avatarPath;
                 //    QString userId;
                 //    QString username;
                 //};
                 User u{"", QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/SuperSubakCache/avatar.png", juser["_id"].toString(), juser["username"].toString()};
                 //"jmess["groupable"].toBool()
                 QJsonArray attachments(jmess["attachments"].toArray());
                 if (attachments.size() > 0) {
                     //qDebug() << "json message :" << jmess << "\n";
                     QUrl lkurl(_serverUrl);
                     QString reply = attachments[0].toObject()["message_link"].toString();
                     QString img = attachments[0].toObject()["image_url"].toString();
                     qDebug() << "------------- jsonattachment: " << attachments[0].toObject();
                     if (img.size()) {
                         lkurl.setPath(img);
                         //QFileInfo check_file("/tmp/SuperSubakCache/" + attachments[0].toObject()["title"].toString());
                         QString filePath("file:/" + QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/SuperSubakCache/" + attachments[0].toObject()["title"].toString());
                         QFileInfo check_file(filePath);
                         if (lkurl.isValid() && !check_file.exists()) {
                             emit downloadImage(lkurl, attachments[0].toObject()["title"].toString());
                         }
                         emit recievedMessage(jmess["_id"].toString(), jmess["msg"].toString(), filePath, u.username, u.userId, 1, reverseMode);
                     } else if (reply.size()){
                         emit recievedMessage(jmess["_id"].toString(), jmess["msg"].toString(), attachments[0].toObject()["text"].toString(), u.username, u.userId, 2, reverseMode);
                     }
                 } else {
                     emit recievedMessage(jmess["_id"].toString(), jmess["msg"].toString(), "", u.username, u.userId, 0, reverseMode);
                 }
                 if (reverseMode) {
                     ++i;
                 } else {
                     --i;
                 }
             }
             replied = true;
             if (jmessageArray.size() != 0) {
                 _selectedChannel.lastMessageTimestamp = jmessageArray[0].toObject()["ts"].toString();
                 _selectedChannel.earliestMessageTimestamp = jmessageArray[jmessageArray.size()-1].toObject()["ts"].toString();
                 //qDebug() << "set lastMessageTimestamp: " << _selectedChannel.lastMessageTimestamp;
                 //qDebug() << "set earliestMessageTimestamp: " << _selectedChannel.earliestMessageTimestamp;
             } else {
                 //qDebug() << "got nothing";
             }
        }
        if (reverseMode) {
#ifdef WIN
            Sleep(1);
#elif UNI
            sleep(1);
#endif
            emit goToEnd();
        }
        //qDebug() << obj;
    });
}

void RocketChat::listAllChannels() {
    qDebug() << "--rcAPI--: getting channel\n";
    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/channels.list", _serverUrl));
    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply](){
        reply->deleteLater();
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj = doc.object();

        if (obj.value("success").toString() == "error") {
            qWarning() << "error: " << obj.value("error").toInt() << "\n";
        } else {
            qDebug() << "info: " << obj.value("info") << "\n";
            for (const QJsonValue& jvchan: obj["channels"].toArray()) {
                QJsonObject jchan(jvchan.toObject());
                emit listAvailableChannel(jchan["_id"].toString(), jchan["name"].toString(), QDateTime::fromString(jchan["_updatedAt"].toString(), Qt::ISODate));
            }
        }
        //qDebug() << obj;
    });
}

// https://rocket.chat/docs/developer-guides/rest-api/subscriptions/read/
// mark Notifications as read

void RocketChat::getSubscriptions() {
    //qDebug() << "\n--rcAPI--: getting subscriptions\n";
    QNetworkRequest restCall(getAuthNetworkRequest("/api/v1/subscriptions.get", _serverUrl));
    QNetworkReply *reply = _qnam.get(restCall);
    connectErrorCallback(reply);
    connect(reply, &QNetworkReply::finished, this, [this, reply](){
        reply->deleteLater();
        const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        const QJsonObject obj = doc.object();

        if (obj.value("success").toString() == "error") {
            qWarning() << "error: " << obj.value("error").toInt() << "\n";
        } else {
            for (const QJsonValue& jvsub: obj["update"].toArray()) {
                QJsonObject jsub(jvsub.toObject());
                //qDebug() << "rid: " << jsub["rid"].toString();
                //qDebug() << "name: " << jsub["name"].toString();
                //qDebug() << "open: " << jsub["open"].toBool();
                //qDebug() << "alert: " << jsub["alert"].toBool();
                //qDebug() << "userMentions: " << jsub["userMentions"].toInt();
                //qDebug() << "groupMentions: " << jsub["groupMentions"].toInt();
                //qDebug() << "_updatedAt: " << jsub["_updatedAt"].toString();
                //qDebug() << "\n";
                emit updateChannel(jsub["rid"].toString(), jsub["userMentions"].toInt() + jsub["groupMentions"].toInt(), QDateTime::fromString(jsub["_updatedAt"].toString(), Qt::ISODate));
            }
        }
        qDebug() << obj;
    });
}

void RocketChat::loadMore() {
    qDebug() << "+++++loading more from: " << _selectedChannel.earliestMessageTimestamp;
    getChannelMessages(_selectedChannel.chanId, GetMessageMode::Oldest, _selectedChannel.earliestMessageTimestamp, _selectedChannel.type);
}

// TODO: should be downloaded once for all locally to download it only once
QString RocketChat::getAssetLogo() {
    return _serverUrl.toString() + "/assets/logo";
}

QString RocketChat::getAssetLogo(QString server) {
    return "https://" + server + "/assets/logo";
}
