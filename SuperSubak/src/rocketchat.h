#ifndef ROCKETCHAT_H
#define ROCKETCHAT_H
#include "enableDebugDraw.h"

#include <QObject>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QAbstractListModel>
#include <QColor>
#include <QTimer>
#include <QDir>
#include <QProcess>
#include "user.h"

class RocketChat: public QObject {
    Q_OBJECT
public:
    enum ChannelType {
        Channel = 0,
        DM = 1,
        Unknown = 2
    };
    QUrl _serverUrl;
private:
    struct ChanSelect {
        QString chanId;
        QString chanName;
        QString earliestMessageTimestamp;
        QString lastMessageTimestamp;
        ChannelType type;
    };
    QNetworkAccessManager _qnam;
    User _me;
    QTimer getMessagesTimer;
    ChanSelect _selectedChannel;
    QNetworkRequest getAuthNetworkRequest(QString, QUrl);
    void connectErrorCallback(QNetworkReply*);
public:
    explicit RocketChat(QObject *parent = nullptr);
    RocketChat(const RocketChat&);
    RocketChat& operator=(const RocketChat&);
    void logFromFile(QString file);
    Q_INVOKABLE void serverInfos();
    Q_INVOKABLE void login(QString, QString, QString);
    Q_INVOKABLE void postmessage(QString);
    enum GetMessageMode {
        Default = 0,
        Oldest = 1,
        Latest = 2
    };
    Q_INVOKABLE void getChannelMessages(QString, GetMessageMode mode, QString, ChannelType);
    Q_INVOKABLE void listChannelMembers(QString);
    Q_INVOKABLE void listChannels();
    Q_INVOKABLE void selectChannel(QString, QString, int);
    Q_INVOKABLE QString selectedChannel();
    Q_INVOKABLE void checkNewMessages();
    Q_INVOKABLE void fallbackOpenLink(QUrl);
    Q_INVOKABLE void listAllChannels();
    Q_INVOKABLE void getSubscriptions();
    Q_INVOKABLE void listAllDMChannels();
    Q_INVOKABLE void loadMore();
    Q_INVOKABLE static QString getCacheFolder();
    Q_INVOKABLE static QString getConfigFolder();
    Q_INVOKABLE QString getAssetLogo();
    Q_INVOKABLE QString getAssetLogo(QString);
signals:
    void logged();
    void loginFailed();
    void addJoinedChannel(QString channelId, QString channelName, QDateTime lastUpdated, int type);
    void listAvailableChannel(QString channelId, QString channelName, QDateTime update);
    void recievedMessage(QString messageId, QString messageString, QString attachmentString, QString authorName, QString userid, int attachmentType, bool oldMode);
    void updateChannel(QString channelId, int notif, QDateTime update);
    void networkError(int errCode, QString call, QString errString);
    void goToEnd();
    void registerUser(QString id, QString name);
private slots:
    void getAvatar(QString);
    void downloadImage(QUrl, QString);
};

#endif // ROCKETCHAT_H
