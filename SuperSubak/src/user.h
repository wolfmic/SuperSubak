#ifndef USER_H
#define USER_H

struct User {
    QString authToken;
    QString avatarPath;
    QString userId;
    QString username;
};

#endif // USER_H
