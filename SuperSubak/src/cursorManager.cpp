#include "cursorManager.h"

CursorManager::CursorManager(QObject *parent) : QObject(parent)
{
}

void CursorManager::set(QPoint p) {
    pos = p;
}

void CursorManager::updateX() {
    if (mainView->visibility() != QWindow::Maximized) {
        mainView->setPosition(QCursor::pos().x() - pos.x(), mainView->position().y());
    }
}

void CursorManager::updateY() {
    if ((QCursor::pos().y() <= 0) && (mainView->visibility() != QWindow::Maximized)) {
        mainView->setPosition(0, 0);
        mainView->showMaximized();
    } else if (mainView->visibility() != QWindow::Maximized) {
        mainView->setPosition(mainView->position().x(), QCursor::pos().y() - pos.y());
    } else if ((mainView->visibility() == QWindow::Maximized) && (QCursor::pos().y() > pos.y())){
        mainView->showNormal();
    }
}
