#ifndef serverListModel_h
#define serverListModel_h

#include "enableDebugDraw.h"
#include "rocketchat.h"

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QObject>
#include <QDateTime>
#include <QAbstractListModel>
#include <QColor>

class ServerListModel : public QAbstractListModel {
    Q_OBJECT
public:
    struct ServerData {
        QString _id;
        QString name;
        QString authToken;
        QColor colorCode;
        QDateTime lastUpdate;
        QString configFilePath;
        int notif;
        int type; // not used yet, will describe if rocketChat, Discord or any other serverType
        RocketChat rc;
    };
    enum Role {
        ID,
        NAME,
        COLORCODE,
        LASTUPDATE,
        NOTIFICATION,
        AUTHTOKEN,
        TYPE
    };
    QList<ServerData> _servers;
    //QList<RocketChat> _serversObjs; // me from the future: replace this to a pointer inside ServerData to avoid any copy operator and delete all the copy operator bullshit
private:
    float currentHue;
public:
    explicit ServerListModel(QObject* parent = Q_NULLPTR);
    virtual ~ServerListModel() override {}
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    Q_SLOT void addServer(QString id, QString name, QDateTime lastUpdate, QString configFile, int type /* not used yet, will describe if rocketChat, Discord or any other serverType*/);
    Q_SLOT void setServer(QString id, int notif, QDateTime lastUpdate);
    Q_INVOKABLE QString getName(int);
    Q_INVOKABLE void clear();
    Q_INVOKABLE QString getId(int);
    Q_INVOKABLE int getType(int index);

   QQmlContext* _qmlContext;
   Q_INVOKABLE void changeServer(int index);
};

#endif
