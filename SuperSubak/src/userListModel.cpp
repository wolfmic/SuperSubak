#include "userListModel.h"

UserListModel::UserListModel(QObject* parent) : QAbstractListModel(parent), currentHue(0.0f) {
}

QHash<int, QByteArray> UserListModel::roleNames() const {
    QHash<int,QByteArray> rez;
    rez[ID]="_id";
    rez[NAME]="name";
    return rez;
}
int UserListModel::rowCount(const QModelIndex &) const {
    return users.size();
}
QVariant UserListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= users.size())
    {
        return QVariant("invalid id");
    }
    UserData entry = users[index.row()];
    if (role == ID) {
        return QVariant(entry._id);
    } else if (role == NAME) {
        return QVariant(entry.name);
    }
    
    
    return QVariant("invalid member name");
}

void UserListModel::clear() {
    beginRemoveRows(QModelIndex(), 0, users.size());
    users.clear();
    endRemoveRows();
}

void UserListModel::addUser(QString id, QString name) {
    users.indexOf({id, name});
    beginInsertRows(QModelIndex(), rowCount(), rowCount());   // kindly provided by superclass
    //name.replace(0, 1, name[0].toUpper());
    // object creation based on params...
    currentHue += 0.618033988749895f;
    currentHue = currentHue - (int)currentHue * 1.0f;
    //currentHue = std::fmod(currentHue, 1.0f);
    UserData c{id, name};
    users << c;
    endInsertRows();
}

// TODO: I should make a Map because of research performances which can be super slow with a lot of channels
void UserListModel::setUser(QString id) {
    UserData* c = nullptr;
    int i = 0;
    for (UserData& x : users) {
        if (x._id == id) {
            c = &x;
            break;
        }
        ++i;
    }
    if (c == nullptr) {
        //qDebug() << "couldn't found channel: " << id;
        return;
    }

    // TODO dunno the preformance impact of this thing
    emit dataChanged(index(i), index(i));
}

QString UserListModel::getName(int index) {
    return users[index].name;
}

QString UserListModel::getId(int index) {
    return users[index]._id;
}

bool UserListModel::UserData::operator==(const UserListModel::UserData& o) const {
    return (o._id == _id) && (o.name == name);
}
