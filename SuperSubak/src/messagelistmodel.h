#ifndef MESSAGELISTMODEL_H
#define MESSAGELISTMODEL_H

#include "enableDebugDraw.h"
#include <QObject>
#include <QAbstractListModel>
#include "user.h"

class MessageListModel: public QAbstractListModel { // placeholder class
    Q_OBJECT
public:
    struct MessageData {
        QString author;
        QString message;
        QString attachment;
        QString userid;
        int type;
    };
    enum Role {
        AUTHOR,
        MESSAGE,
        // everything should be replaced by userID and reference to this userid using the map in RocketChat::
        USERID,
        MERGEDNAMEID,
        ATTACHMENT,
        TYPE
    };
private:
    // TODO: having a list for every channel so we could switch easily and make better offline navigation
    QList<MessageData> messages;
    void markdown2RichText(QString& string);
    void sanitiseReply(QString& str);
public:
    explicit MessageListModel(QObject* parent = Q_NULLPTR);
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    //Q_SLOT void addMessage(QString messageId, QString message, User userdata);
    Q_INVOKABLE void clear();
    Q_SLOT void addMessage(QString messageId, QString message, QString attachment, QString authorName, QString userid, int attachmentType, bool oldMode);
};

#endif // MESSAGELISTMODEL_H
