#include "channellistmodel.h"
#include <QDebug>

ChannelListModel::ChannelListModel(QObject* parent) : QAbstractListModel(parent), currentHue(0.0f) {}

QHash<int, QByteArray> ChannelListModel::roleNames() const {
    QHash<int,QByteArray> rez;
    rez[ID]="_id";
    rez[NAME]="name";
    rez[COLORCODE]="colorCode";
    rez[LASTUPDATE]="lastUpdate";
    rez[NOTIFICATION]="notificationAmount";
    rez[TYPE]="type";
    return rez;
}
int ChannelListModel::rowCount(const QModelIndex &) const {
    return channels.size();
}
QVariant ChannelListModel::data(const QModelIndex &index, int role) const {
        if (index.row() < 0 || index.row() >= channels.size())
        {
            return QVariant("invalid id");
        }
        ChannelData entry = channels[index.row()];
        if (role == ID) {
            return QVariant(entry._id);
        } else if (role == NAME){
            return QVariant(entry.name);
        } else if (role == COLORCODE) {
            return QVariant::fromValue(entry.colorCode);
        } else if (role == LASTUPDATE) {
            return QVariant::fromValue(entry.lastUpdate.toString(Qt::ISODate));
        } else if (role == NOTIFICATION) {
            return QVariant::fromValue(entry.notif);
        } else if (role == TYPE) {
            return QVariant::fromValue(entry.type);
        }


        return QVariant("invalid member name");
}

void ChannelListModel::clear() {
    beginRemoveRows(QModelIndex(), 0, channels.size());
    channels.clear();
    endRemoveRows();
}

void ChannelListModel::addChannel(QString id, QString name, QDateTime lastUpdate, int type) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());   // kindly provided by superclass
    //name.replace(0, 1, name[0].toUpper());
    // object creation based on params...
    currentHue += 0.618033988749895f;
    currentHue = currentHue - (int)currentHue * 1.0f;
    //currentHue = std::fmod(currentHue, 1.0f);
    ChannelData c{id, name, QColor::fromHslF(currentHue, 0.3, 0.3), lastUpdate, 0, type};
    channels << c;
    endInsertRows();
}

// TODO: I should make a Map because of research performances which can be super slow with a lot of channels
void ChannelListModel::setChannel(QString id, int notif, QDateTime lastUpdate) {
    ChannelData* c = nullptr;
    int i = 0;
    for (ChannelData& x : channels) {
        if (x._id == id) {
            c = &x;
            break;
        }
        ++i;
    }
    if (c == nullptr) {
        //qDebug() << "couldn't found channel: " << id;
        return;
    }
    c->notif = notif;
    c->lastUpdate = lastUpdate;
    // TODO dunno the preformance impact of this thing
    emit dataChanged(index(i), index(i));
}

QString ChannelListModel::getName(int index) {
    return channels[index].name;
}

QString ChannelListModel::getId(int index) {
    return channels[index]._id;
}

int ChannelListModel::getType(int index) {
    return channels[index].type;
}
