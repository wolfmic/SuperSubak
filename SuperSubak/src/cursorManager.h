#ifndef CURSORPOSITION_H
#define CURSORPOSITION_H
#include "enableDebugDraw.h"
#include <QObject>
#include <QPoint>
#include <QtQuick>
#include <QCursor>

class CursorManager : public QObject {
    Q_OBJECT
public:
    QQuickWindow* mainView;
    QPoint pos;
    explicit CursorManager(QObject *parent = nullptr);
    virtual ~CursorManager() = default;

    Q_INVOKABLE void set(QPoint);
    Q_INVOKABLE void updateX();
    Q_INVOKABLE void updateY();
};
#endif // CURSORPOSITION_H
